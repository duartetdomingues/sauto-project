#!/usr/bin/env python3
import rospy
import datetime
from msg_py.msg import TwistMessage

def callback(data):
    rospy.loginfo("Mensagem recebida: linear.x = %f, angular.z = %f", data.linear.x, data.angular.z)

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/cmd_vel', TwistMessage, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
