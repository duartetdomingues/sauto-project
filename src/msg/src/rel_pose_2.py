#!/usr/bin/env python3
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import LaserScan
from msg.msg import Points
import math
import time
import numpy as np
import matplotlib.pyplot as plt



class Node:
    def __init__(self):

        self.x = []
        self.y = []
        self.theta = []
        self.t = []
        self.x.append(-6.140441417694092)
        self.y.append(-0.11767883598804474)
        self.theta.append(2*math.acos(0.464569091796875))
        self.cov = np.diag([1., 1., 0., 0., 0., 1.])
        self.t.append(time.time())
        self.i = 0

        rospy.init_node("rel_pose")

        self.pose_pub = rospy.Publisher("/rel_pose", PoseWithCovarianceStamped, queue_size=10)

        self.map_pub = rospy.Publisher("/map_points", Points, queue_size=10)
    
        self.odom_sub = rospy.Subscriber("/odom", Odometry, callback = self.pose_callback)

        self.acml_pose_sub = rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, self.amcl_pose_callback)

        self.map_sub = rospy.Subscriber("/map", OccupancyGrid, self.map_callback)

        self.lidar_sub = rospy.Subscriber("/scan", LaserScan, callback = self.lidar_callback)



    def pose_callback(self, msg: Odometry):
        self.i = self.i+1

        self.v = msg.twist.twist.linear.x
        self.w = msg.twist.twist.angular.z

        self.predict()
        self.rel_pose_pub()


       
    def predict(self):
        self.t.append(time.time())
        self.delta_t = self.t[self.i]-self.t[self.i-1]

        self.theta.append(self.theta[self.i-1] + self.w*self.delta_t)
        self.x.append(self.x[self.i-1] + self.v*self.delta_t*math.cos(self.theta[self.i]))
        self.y.append(self.y[self.i-1] + self.v*self.delta_t*math.sin(self.theta[self.i]))

        
        G = np.array([[1., 0., 0., 0., 0., -self.v*self.delta_t*math.sin(self.theta[self.i]+self.w*self.delta_t)],[0., 1., 0., 0., 0., -self.v*self.delta_t*math.cos(self.theta[self.i]+self.w*self.delta_t)], np.zeros(6), np.zeros(6), np.zeros(6),[0., 0., 0., 0., 0., 1.]])
        Gt = np.transpose(G)

        self.cov = G @ self.cov @ Gt

    def update(self):
        i = 0
        j = 0
        self.lidar_points = np.empty((0,2))
        self.map_points = np.empty((0,2))

        for i in range(len(self.lidar_data)):

            if self.lidar_data[i] >= 0.12 and self.lidar_data[i] <= 3.5:

                p_l = self.lidar_data[i]
                theta_l = i*self.lidar_increment

                self.lidar_points = np.append(self.lidar_points, [[p_l, theta_l]], axis = 0)
                min = 2147483647

                for j in range(len(self.map_data)):
                    if self.map_data[j] == 100:
                        x, y = self.pixel_to_distance(j)
                        p = math.sqrt((x-self.x[self.i])**2 + (y-self.y[self.i])**2)
                        if x-self.x[self.i] == 0 and y-self.y[self.i] > 0:
                            theta = math.pi/2 - self.theta[self.i]
                        elif x-self.x[self.i] == 0 and y-self.y[self.i] < 0:
                            theta = -math.pi/2 - self.theta[self.i]
                        elif x-self.x[self.i] == 0 and y-self.y[self.i] == 0:
                            theta = -self.theta[self.i]
                        elif x-self.x[self.i] > 0:
                            theta = math.atan2(y-self.y[self.i], x-self.x[self.i]) - self.theta[self.i]
                        else:
                            theta = math.atan2(y-self.y[self.i], x-self.x[self.i]) + math.pi - self.theta[self.i]

                        if theta < 0:
                            theta = theta + 2*math.pi

                        aux = abs(((p-p_l)/p_l)*((theta-theta_l)/theta_l))

                        if aux < min:
                            min = aux
                            map_point = [p, theta]


                self.map_points = np.append(self.map_points, [map_point], axis = 0)
                            
                        
        


    def amcl_pose_callback(self, msg: PoseWithCovarianceStamped):
        self.amcl_x= msg.pose.pose.position.x
        self.amcl_y= msg.pose.pose.position.y
        self.amcl_theta= 2*math.acos(msg.pose.pose.orientation.w)
        self.amcl_cov= msg.pose.covariance
    
    def rel_pose_pub(self):
        self.pose_msg = PoseWithCovarianceStamped()
        self.pose_msg.header.frame_id = 'map'
        self.pose_msg.pose.pose.position.x = self.x[self.i]
        self.pose_msg.pose.pose.position.y = self.y[self.i]
        self.pose_msg.pose.pose.orientation.z = math.sin(self.theta[self.i]/2)
        self.pose_msg.pose.pose.orientation.w = math.cos(self.theta[self.i]/2)
        self.pose_msg.pose.covariance = self.cov.flatten().tolist()

        self.pose_pub.publish(self.pose_msg)

    def map_callback(self, msg: OccupancyGrid):
        self.map_res = msg.info.resolution
        self.map_width = msg.info.width
        self.map_height = msg.info.height
        self.map_origin_x = msg.info.origin.position.x
        self.map_origin_y = msg.info.origin.position.y
        self.map_origin_theta = 2*math.acos(msg.info.origin.orientation.w)
        self.map_data = msg.data

    def lidar_callback(self, msg: LaserScan):
        
        self.lidar_increment = msg.angle_increment
        self.lidar_data = msg.ranges

        rospy.loginfo("lidar callbacked")

        self.update()

        #rospy.loginfo(self.lidar_points)
        #rospy.loginfo(self.map_points)

        self.pub_map_points()



    def pixel_to_distance(self, number):
        y = number//self.map_width
        x = number - y*self.map_width

        x = self.map_origin_x + x*self.map_res
        y = self.map_origin_y + y*self.map_res

        return x, y
    
    def pub_map_points(self):
        msg = Points()
        msg.lidar_points = np.array(self.lidar_points).flatten().tolist()
        msg.map_points = np.array(self.map_points).flatten().tolist()



        self.map_pub.publish(msg)


if __name__ == "__main__":

    node = Node()
    rospy.spin()
    #plt.plot(node.x, node.y, color = 'g')
    #plt.plot(node.amcl_x, node.amcl_y, color = 'r')
    #plt.show()